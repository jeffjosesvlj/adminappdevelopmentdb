package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.Validation;

/**
 * Created by root on 3/2/16.
 */
public class ProfileFragment  extends Fragment {

    EditText userName, emailId, password, confirmPassword, phoneNumber, city;
    String name, email, phone, cit;
    Button save;
    String phnumber;
    View rootView;
    Master master;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.profile, container, false);
        getActivity().setTitle(R.string.title_settings);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        userName = (EditText)rootView.findViewById(R.id.username);
        emailId = (EditText)rootView.findViewById(R.id.email);
        password = (EditText)rootView.findViewById(R.id.password);
        confirmPassword = (EditText)rootView.findViewById(R.id.confirmpassword);
        phoneNumber = (EditText)rootView.findViewById(R.id.contact);
        city = (EditText)rootView.findViewById(R.id.city);
        save = (Button)rootView.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null && Master.isNetworkAvailable(getActivity())) {
                    if (name.equals(userName.getText().toString().trim())
                            && email.equals(emailId.getText().toString().trim())
                            && phone.equals(phoneNumber.getText().toString().trim())
                            && cit.equals(city.getText().toString().trim())
                            && password.getText().toString().trim().equals("")
                            && confirmPassword.getText().toString().trim().equals(""))
                        Toast.makeText(getActivity(), R.string.label_toast_no_changes_to_save, Toast.LENGTH_SHORT).show();
                    else if (checkValidation())
                        updateProfile();
                    else
                        Toast.makeText(getActivity(), R.string.label_toast_Form_contains_error, Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getActivity(),R.string.label_toast_no_network_connection,Toast.LENGTH_SHORT).show();
                }
            }

        });
        new GetProfileTask(getActivity()).execute();

    }

    private void updateProfile() {
        JSONObject profileObj = new JSONObject();
        try
        {
            profileObj.put("name", userName.getText().toString().trim());
            profileObj.put("email", emailId.getText().toString().trim());
            profileObj.put("address", city.getText().toString().trim());
            profileObj.put("phonenumber", "91"+ AdminDetails.getMobileNumber().trim());
            profileObj.put("newnumber", "91"+phoneNumber.getText().toString().trim());
            if(!password.getText().toString().trim().equals("")&&confirmPassword.getText().toString().trim().equals(""))
            {
                Validation.hasText(getActivity(), confirmPassword);
            }
            else if(password.getText().toString().trim().equals("")&&!confirmPassword.getText().toString().trim().equals(""))
            {
                Validation.hasText(getActivity(), password);
            }
            else if(!password.getText().toString().trim().equals("")&&!confirmPassword.getText().toString().trim().equals(""))
            {
                if(Validation.isPasswordMatch(getActivity(), confirmPassword, password.getText().toString().trim(), true))
                {
                    profileObj.put("password", password.getText().toString().trim());
                    if(Master.isNetworkAvailable(getActivity()))
                        new UpdateProfileTask(getActivity()).execute(profileObj);
                    else
                        Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
            else if(Master.isNetworkAvailable(getActivity()))
                new UpdateProfileTask(getActivity()).execute(profileObj);
            else
                Toast.makeText(getActivity(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

//-----------------------------get profile task---------------------------------------------------------------

    public class  GetProfileTask extends AsyncTask<String,String, String> {

        ProgressDialog pd;
        Context context;
        String response;
        public GetProfileTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String profileURL = master.serverURL + "api/" + AdminDetails.getAbbr()+"/profilesettingsview?number=91"+AdminDetails.getMobileNumber();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(profileURL,null, "GET",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response1) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(ProfileFragment.this.isAdded()){
                if(response1.equals("exception"))
                {
                    Toast.makeText(getActivity(), R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later, Toast.LENGTH_SHORT);
                }
                else
                {
                    try {
                        JSONObject resObj = new JSONObject(response1);
                        if(resObj.getString("response").equals("success")) {
                            userName.setText(resObj.getString("name"));
                            city.setText(resObj.getString("address"));
                            phnumber=resObj.getString("phonenumber");
                            phnumber= phnumber.substring(2);
                            phoneNumber.setText(phnumber);
                            emailId.setText(resObj.getString("email"));

                            name = resObj.getString("name");
                            cit = resObj.getString("address");
                            phone = resObj.getString("phonenumber").substring(2);
                            email = resObj.getString("email");
                        }
                        else {
                            Toast.makeText(getActivity(), resObj.getString("Error")+"Sorry, please try again later", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
//----------------------------------------------------------------------------------------------------------


//-----------------------------------update profile---------------------------------------------------

    public class  UpdateProfileTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String response;
        JSONObject jsonObject;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;

        public UpdateProfileTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            jsonObject = params[0];
            String profileURL = master.serverURL + "api/" + AdminDetails.getAbbr()+"/profilesettingsupdate";
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(profileURL, params[0], "POST",true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response1) {

            if(pd != null && pd.isShowing())
                pd.dismiss();

            if(ProfileFragment.this.isAdded()){
                if(response1.equals("exception"))
                {
                    Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later), "OK");
                }
                else
                {
                    try
                    {
                        JSONObject resObj = new JSONObject(response1);
                        if(resObj.getString("response").equals("Successfully updated"))
                        {
                            Master.alertDialog(getActivity(), getString(R.string.label_alertdialog_Your_profile_updated_successfully), "OK");
                            ArrayList<TextView> textViews = DashboardActivity.getDrawerTextViews();
                            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                            editor = sharedPreferences.edit();
                            if(AdminDetails.getEmail() != jsonObject.getString("email"))
                            {
                                AdminDetails.setEmail(jsonObject.getString("email"));
                                textViews.get(1).setText(AdminDetails.getEmail());
                                editor.putString("emailid", AdminDetails.getEmail());
                            }
                            if(AdminDetails.getMobileNumber() != jsonObject.getString("newnumber"))
                            {
                                AdminDetails.setMobileNumber(jsonObject.getString("newnumber").substring(2));
                                textViews.get(2).setText(AdminDetails.getMobileNumber());
                                editor.putString("mobilenumber", AdminDetails.getMobileNumber());
                            }
                            if(jsonObject.has("password") && AdminDetails.getPassword() != jsonObject.get("password"))
                            {
                                AdminDetails.setPassword(jsonObject.getString("password"));
                                editor.putString("password", AdminDetails.getPassword());
                            }
                            editor.commit();

                            phone = jsonObject.getString("newnumber").substring(2);
                            email = jsonObject.getString("email");
                            name = jsonObject.getString("name");
                            cit = jsonObject.getString("address");
                        }
                        else
                        {
                            Toast.makeText(getActivity(), resObj.getString("Error")+"Sorry, please try again later", Toast.LENGTH_SHORT).show();
                            if(resObj.getString("Error").equals("Email Exists"))
                                emailId.setError("Email already exists");
                            if(resObj.getString("Error").equals("Phonenumber Exists"))
                                phoneNumber.setError("Mobile number already exists");
                        }
                    }
                    catch (JSONException e)
                    {
                        Toast.makeText(getActivity(), R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }
    }
//------------------------------------------------------------------------------------------------------
    private boolean checkValidation()
    {
        boolean ret = true;
        if (!Validation.hasText(getActivity(), userName)) ret = false;
        if (!Validation.isEmailAddress(getActivity(), emailId, true, "emailid")) ret = false;
        if (!Validation.isMobileNumber(getActivity(), phoneNumber, true, "mobileno"))ret=false;
        if (!Validation.hasText(getActivity(), city)) ret = false;
        return ret;
    }

   //--------------------------------------------------------------------------------------------------//

}