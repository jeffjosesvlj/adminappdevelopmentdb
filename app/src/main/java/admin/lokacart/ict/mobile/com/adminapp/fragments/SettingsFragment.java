package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.PagerAdapter;
import admin.lokacart.ict.mobile.com.adminapp.R;


/**
 * Created by Vishesh on 18-01-2016.
 */
public class SettingsFragment extends Fragment {

    PagerAdapter mPageAdapter;
    ViewPager viewPager;
    FragmentManager fm;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        if(SettingsFragment.this.isAdded()) {
            UnClickChangeColor();
        }

        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
        getActivity().setTitle(R.string.title_settings);
        setRetainInstance(true);
        final TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_profile_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_billlayout_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_general_settings)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        fm = getFragmentManager();
        mPageAdapter = new PagerAdapter(fm, tabLayout.getTabCount(),"Setting");
        viewPager = (ViewPager)rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                DashboardActivity.resetBackPress();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void UnClickChangeColor() {
        // Initial colors of each system bar.
        final int statusBarColor = getResources().getColor(R.color.toColorPrimary);
        final int toolbarColor = getResources().getColor(R.color.toColorPrimaryDark);

        // Desired final colors of each bar.
        final int statusBarToColor = getResources().getColor(R.color.colorPrimary);
        final int toolbarToColor = getResources().getColor(R.color.colorPrimaryDark);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();

                // Apply blended color to the status bar.
                int blended = blendColors(statusBarColor, statusBarToColor, position);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && getActivity() != null) {
                    getActivity().getWindow().setStatusBarColor(blended);
                }

                // Apply blended color to the ActionBar.
                blended = blendColors(toolbarColor, toolbarToColor, position);
                ColorDrawable background = new ColorDrawable(blended);
                if(getActivity() != null){
                    ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(background);
                }
            }
        });

        anim.setDuration(150).start();
    }



    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
}
